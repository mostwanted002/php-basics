<?php
    
    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        if($_POST['signUp']){
            $username = $_POST['username'];
            $password = $_POST['password'];
            
            addUser($username, $password);
        }
        else verifyLogin($_POST['username'], $_POST['password']);
    }
    
    function addUser($username, $password){
        $password = hash('sha256',$password);
        $dbhost = 'localhost';
        $dbuser = 'php-practical';
        $dbpass = 'php12345';
        $db = 'PHP_BASICS';
        $conn = new mysqli($dbhost, $dbuser, $dbpass, $db);
        $query = $conn->prepare('INSERT INTO users VALUES(?, SHA2(?,0));');
        $query->bind_param('ss', $username, $password);
        if(!$query){
            die('Couldn\'t add user: ' . $conn->connect_error);
        }
        $query->execute();
        echo "User: $username added successfully!";
    }
    function verifyLogin($username, $password){
        $password = hash('sha256',$password);
        $dbhost = 'localhost';
        $dbuser = 'php-practical';
        $dbpass = 'php12345';
        $db = 'PHP_BASICS';
        $conn = new mysqli($dbhost, $dbuser, $dbpass, $db);
        $stmt = $conn->prepare('SELECT password FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($row = $result->fetch_array()){
            if ($row["password"] == $password)
                echo 'Welcome user! Here\'s your flag: ROOTERS_CTF{bl1nd_5ql}';
            else die('Wrong password :/');
        }
    }
?>
