<?php 
    $present = getdate();
    
    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        $bday = $_POST["day"];
        $bmonth = $_POST["month"];
        $byear = $_POST["year"];
        if(checkdate((int)$bmonth, (int)$bday, (int)$byear)){
            $tday = getdate();
            $today = date_create($tday["year"].'-'.$tday["mon"].'-'.$tday["mday"]);
            $birthday = date_create($tday["year"].'-'.$bmonth.'-'.$bday);
            $countdown = date_diff($today, $birthday);
            if ($countdown->invert)
                echo 'Your birthday is '.(365 - $countdown->days).' days away! :)';
            else
                echo 'Your birthday is '.($countdown->days).' days away! :)';
        }
        else
            echo 'Dude! Are you alien or something!?';
    }
?>
