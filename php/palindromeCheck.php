<?php
    
    $string = "";
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $string = $_POST["str"];
        palindromeCheck($string);
    }
    
    function palindromeCheck($string){
        $string = strtolower($string);
        $strt = explode(" ",$string);
        $strtocheck = '';
        for($i = 0; $i < count($strt); $i++){
            $strtocheck .= $strt[$i];
        }
        $flag = 0;
        for($i = 0, $j = strlen($strtocheck) - 1; $i <= (strlen($strtocheck)/2); $i++, $j--){
            if($strtocheck[$i] != $strtocheck[$j]){
                $flag++;
                break;
            }
        }
        if($flag != 0)
            echo "'$string' is Not a Palindrome.";
        else
            echo "'$string' is a Palindrome.";
    }
    
?>
