<?php
    
    $number = "";
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $number = $_POST["num"];
        primeCheck($number);
    }
    
    function primeCheck($number){
        $flag = 0;
        for($i = 2; $i <= sqrt($number); $i++){
            if($number%$i == 0){
                $flag++;
                break;
            }
        }
        if($flag != 0)
            echo "$number is Not a Prime.";
        else
            echo "$number is a Prime.";
    }
    
?>
