<?php
    
    $number = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $number = $_POST["num"];
        echo "Factorial of $number is " . factorial($number);
    }
    
    function factorial($number){
        if ($number <= 1){
            return 1;
        }
        else {
            return ($number * factorial($number-1));
        }
    }

?>
